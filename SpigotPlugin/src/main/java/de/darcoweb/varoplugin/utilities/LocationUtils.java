package de.darcoweb.varoplugin.utilities;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class LocationUtils {

    public static Location getCenter(Location location) {
        return new Location(
                location.getWorld(),
                location.getBlockX() + 0.5,
                location.getBlockY() + 0.5,
                location.getBlockZ() + 0.5,
                location.getYaw(),
                location.getPitch()
        );
    }

    public static Location getBlockCenter(Location location) {
        return new Location(
                location.getWorld(),
                location.getBlockX() + 0.5,
                location.getBlockY() + 0.5,
                location.getBlockZ() + 0.5
        );
    }

    public static Location getSurfaceCenter(Location location) {
        return new Location(
                location.getWorld(),
                location.getBlockX() + 0.5,
                location.getBlockY(),
                location.getBlockZ() + 0.5
        );
    }

    public static class EntityUtils {
        // ArmorStandUtils
        public static ArmorStand makeMarker(ArmorStand armorStand) {
            armorStand.setGravity(false);
            armorStand.setVisible(false);
            armorStand.setMarker(true);

            return armorStand;
        }
    }
}
