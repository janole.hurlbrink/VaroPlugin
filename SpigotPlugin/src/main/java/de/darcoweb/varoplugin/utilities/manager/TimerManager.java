package de.darcoweb.varoplugin.utilities.manager;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.runnables.DailyTimer;
import de.darcoweb.varoplugin.runnables.StartTimer;
import de.darcoweb.varoplugin.utilities.TimerTuple;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

@SuppressWarnings({"PointlessArithmeticExpression", "unused"})
public class TimerManager {

    private BukkitTask startTimerTask;
    private BukkitTask dailyTimerTask;
    private HashMap<UUID, TimerTuple> tracked = new HashMap<>();

    public TimerManager() {
        loadTracked();
        initiateDailyTimerTask();
    }

    @SuppressWarnings("unchecked")
    public void loadTracked() {
        FileManager fileManager = VaroPlugin.getInstance().getFileManager();

        if (fileManager.contains(FileManager.YamlFile.TIMER, "tracked_uuids")) {
            List<String> uuids = (List<String>) fileManager.getFileConfig(FileManager.YamlFile.TIMER).getList("tracked_uuids");

            for (String uuidString : uuids) {
                int timeLeft = (Integer) fileManager.get(FileManager.YamlFile.TIMER, uuidString + ".time_left");
                int invincLeft = (Integer) fileManager.get(FileManager.YamlFile.TIMER, uuidString + ".invincibility_left");

                UUID uuid = UUID.fromString(uuidString);
                TimerTuple tt = new TimerTuple(Bukkit.getOfflinePlayer(uuid), timeLeft, invincLeft);

                this.tracked.put(uuid, tt);
            }
        }
    }

    public void saveTracked() {
        FileManager fileManager = VaroPlugin.getInstance().getFileManager();
        fileManager.freshConfig(FileManager.YamlFile.TIMER);
        if (!tracked.isEmpty()) {
            List<String> trackedUUIDs = new ArrayList<>();
            for (Entry<UUID, TimerTuple> entry : tracked.entrySet()) {
                UUID uuid = entry.getKey();
                trackedUUIDs.add(uuid.toString());

                int timeLeft = entry.getValue().getTimeLeft();
                int invincLeft = entry.getValue().getInvincibleLeft();

                fileManager.write(FileManager.YamlFile.TIMER, uuid + ".time_left", timeLeft);
                fileManager.write(FileManager.YamlFile.TIMER, uuid + ".invincibility_left", invincLeft);
            }
            fileManager.write(FileManager.YamlFile.TIMER, "tracked_uuids", trackedUUIDs);
        }
    }

    public void initiateDailyTimerTask() {
        DailyTimer dailyTimerInstance = new DailyTimer();
        setDailyTimerTask(Bukkit.getScheduler().runTaskTimer(VaroPlugin.getInstance(), dailyTimerInstance, 0L, 1 * 20L));
    }

    // Methods for the players themselves (join)
    public void newTrackedPlayer(VaroPlugin plugin, OfflinePlayer p, boolean doInvincible) {
        TimerTuple tt = new TimerTuple(p, plugin.getConfig(), doInvincible);

        this.tracked.put(p.getUniqueId(), tt);
    }

    public boolean isTracked(OfflinePlayer p) {
        return this.tracked.containsKey(p.getUniqueId());
    }

    public void removeTrackedPlayer(OfflinePlayer p) {
        this.tracked.remove(p.getUniqueId());
    }

    public TimerTuple getValueForPlayer(OfflinePlayer p) {
        return this.tracked.get(p.getUniqueId());
    }

    public TimerTuple getValueForPlayer(UUID uuid) {
        return this.tracked.get(uuid);
    }

    // -----------

    // Methods for the Start Timer (/start)
    public void runStartTimer(VaroPlugin plugin, StartTimer startTimer) {
        setStartTimerTask(Bukkit.getScheduler().runTaskTimer(plugin, startTimer, 0L, 1 * 20L));
    }

    public void cancelStartTimer() {
        startTimerTask.cancel();
        setStartTimerTask(null);
    }

    // -----------

    // Getter and Setter methods
    public/* Concurrent */HashMap<UUID, TimerTuple> getTracked() {
        return tracked;
    }

    private void setDailyTimerTask(BukkitTask task) {
        this.dailyTimerTask = task;
    }

    private void setStartTimerTask(BukkitTask task) {
        this.startTimerTask = task;
    }
}
