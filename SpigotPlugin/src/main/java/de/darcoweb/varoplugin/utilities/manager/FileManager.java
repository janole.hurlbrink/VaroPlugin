package de.darcoweb.varoplugin.utilities.manager;

import de.darcoweb.varoplugin.utilities.Chat;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class FileManager {

	public FileManager() {
		loadAllFiles();
	}

	public enum YamlFile {
		VARO_DATA(null, "VaroData", null), TEAMS(null, "Teams", null), TIMER(null, "Timers", null), BANS(null, "Bans",
				null);

		private File file;
		private String name;
		private FileConfiguration fileConfig;

		YamlFile(File file, String name, FileConfiguration fileConfig) {
			this.file = file;
			this.name = name;
			this.fileConfig = fileConfig;
		}

		public File getFile() {
			return this.file;
		}

		public void setFile(File f) {
			this.file = f;
		}

		public String getName() {
			return this.name;
		}

		public FileConfiguration getFileConfig() {
			return this.fileConfig;
		}

		public void setFileConfig(FileConfiguration fileConfig) {
			this.fileConfig = fileConfig;
		}
	}

	// This method HAS TO BE CALLED BEFORE ANY OTHER.
	public void loadAllFiles() {
		for (YamlFile yf : YamlFile.values()) {
			yf.setFile(newFile(yf.getName()));
			yf.setFileConfig(loadFile(yf.getFile()));
		}
	}
	
	public void freshConfig(YamlFile yf) {
		yf.setFileConfig(new YamlConfiguration());
	}

	public void saveAllFiles() {
		for (YamlFile yf : YamlFile.values()) {
			saveFile(yf);
		}
	}

	public void saveFile(YamlFile yf) {
		saveFile(yf.getFileConfig(), yf.getFile());
	}

	public void write(YamlFile yf, String path, Object value) {
		write(yf.getFileConfig(), yf.getFile(), path, value);
	}

	public Object get(YamlFile yf, String path) {
		return yf.getFileConfig().get(path);
	}

	public boolean contains(YamlFile yf, String path) {
		return yf.getFileConfig().contains(path);
	}

	// THIS SHOULD ONLY BE USED IF YOU WANT TO CALL A FILECONFIGURATION-METHOD
	// IF THERE IS NO SPECIFIC METHOD IN THIS CLASS!
	public FileConfiguration getFileConfig(YamlFile yf) {
		return yf.getFileConfig();
	}

	// ======================
	// FUNCTIONALITY METHODS:
	// ======================
	private File newFile(String name) {
		Plugin plugin = Bukkit.getPluginManager().getPlugin("VaroPlugin");

		File file = new File(plugin.getDataFolder(), name + ".yml");
		if (!file.exists()) {
			try {
				plugin.getDataFolder().mkdir();
				file.createNewFile();
			} catch (IOException e) {
				Chat.err.serverBroadcast("Fehler beim Erstellen von " + file.getName() + " !");
				e.printStackTrace();
			}
		}
		return file;
	}

	private YamlConfiguration loadFile(File file) {
		return YamlConfiguration.loadConfiguration(file);
	}

	private boolean saveFile(FileConfiguration configuration, File file) {
		try {
			configuration.save(file);
		} catch (IOException e) {
			Chat.err.serverBroadcast("Fehler beim Speichern von " + file.getName() + " !");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private FileConfiguration write(FileConfiguration configuration, File file, String path, Object value) {
		configuration.set(path, value);
		saveFile(configuration, file);

		// Is that really neccessary?
		return configuration;
	}
}
