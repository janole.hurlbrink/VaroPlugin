package de.darcoweb.varoplugin.utilities;

import de.darcoweb.varoplugin.VaroPlugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class VaroPluginLocals {

    // Path to german message file: /plugins/VaroPlugin/locals/messagesDE
    private static final String name = "messages";
    private File pluginFolder = new File(VaroPlugin.getInstance().getDataFolder().getPath());

    private Properties locals = new Properties();

    public VaroPluginLocals() {
        loadMessages(VaroPlugin.getInstance().getConfig().getString("language"));
    }

    public String getMessage(String key) {
        return locals.getProperty(key);
    }

    public boolean loadMessages(String localCode) {
        String fileName = "locals/" + name + localCode + ".lang";
        File msgFile = new File(pluginFolder, fileName);

        if (!msgFile.exists()) {
            // Whether to override or not doesn't matter as the file doesn't exist anyway.
            VaroPlugin.getInstance().saveResource(fileName, false);
        }

        try {
            locals.load(new BufferedReader(new FileReader(msgFile)));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
