package de.darcoweb.varoplugin.utilities.manager;

import de.darcoweb.varoplugin.VaroPlugin;
import org.bukkit.OfflinePlayer;

import java.util.*;
import java.util.Map.Entry;

public class BanManager {

	// BAN_REASONS:
	public static int OUT_OF_GAME = 100;
	public static int PLAYED_TODAY = 101;

	// HashMap<UUID playerUUID, Long endOfBan>
	private HashMap<UUID, Long> bannedPlayers = new HashMap<>();

	public BanManager() {
		loadBans();
	}

	public void loadBans() {
		FileManager fileManager = VaroPlugin.getInstance().getFileManager();
		if (fileManager.contains(FileManager.YamlFile.BANS, "uuids")) {
			List<String> uuids = fileManager.getFileConfig(FileManager.YamlFile.BANS).getStringList("uuids");

			for (String uuid : uuids) {
				String endOfBanOrNullString = (String) fileManager.get(FileManager.YamlFile.BANS, uuid);

				Long endOfBanOrNull;
				if (endOfBanOrNullString.equalsIgnoreCase("null"))
					endOfBanOrNull = null;
				else
					endOfBanOrNull = Long.valueOf(endOfBanOrNullString);

				bannedPlayers.put(UUID.fromString(uuid), endOfBanOrNull);
			}
		}
	}

	public void saveBans() {
		FileManager fileManager = VaroPlugin.getInstance().getFileManager();
		fileManager.freshConfig(FileManager.YamlFile.BANS);
		if (!bannedPlayers.isEmpty()) {
			List<String> uuids = new ArrayList<>();

			for (Entry<UUID, Long> entry : bannedPlayers.entrySet()) {
				UUID uuid = entry.getKey();
				Long endOfBanOrNull = entry.getValue();

				uuids.add(uuid.toString());
				fileManager.write(FileManager.YamlFile.BANS, uuid.toString(), String.valueOf(endOfBanOrNull));
			}

			fileManager.write(FileManager.YamlFile.BANS, "uuids", uuids);
		}
	}

	private void update(OfflinePlayer p) {
		if (bannedPlayers.containsKey(p.getUniqueId())) {
			Long endOfBanMillis = bannedPlayers.get(p.getUniqueId());
			if (endOfBanMillis != null) {
				long difference = endOfBanMillis - System.currentTimeMillis();
				if (difference <= 0) {
					bannedPlayers.remove(p.getUniqueId());
				}
			}
			// If endOfBanMillis was null, the player is banned forever. >:D
		}
	}

	public void addBan(OfflinePlayer p, int reason) {
		if (reason == OUT_OF_GAME) {
			// 'null' here indicates there is no end for this ban.
			bannedPlayers.put(p.getUniqueId(), null);
		} else if (reason == PLAYED_TODAY) {
			Calendar c = Calendar.getInstance();
			c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH) + 1, 0, 0, 0);

			long endMillis = c.getTimeInMillis();
			bannedPlayers.put(p.getUniqueId(), endMillis);
		}
	}

	public boolean isBanned(OfflinePlayer p) {
		update(p);
		return bannedPlayers.containsKey(p.getUniqueId());
	}

	public boolean isBannedForever(OfflinePlayer p) {
		if (bannedPlayers.containsKey(p.getUniqueId())) {
			Long endOfBanOrNull = bannedPlayers.get(p.getUniqueId());
			return endOfBanOrNull == null;
		}
		return false;
	}

	public boolean isOutOfGame(OfflinePlayer p) {
		return isBannedForever(p);
	}
}
