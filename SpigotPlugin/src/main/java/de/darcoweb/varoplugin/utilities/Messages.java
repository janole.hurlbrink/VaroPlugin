package de.darcoweb.varoplugin.utilities;


import de.darcoweb.varoplugin.annotations.MessageList;
import de.darcoweb.varoplugin.annotations.impl.MessageInfo;

/**
 * This enum supplies the information about all messages to the {@link de.darcoweb.varoplugin.annotations.processors.MessageListProcessor MessageListProcessor}.
 *
 * @author 1Darco1
 */
@SuppressWarnings("unused")
@MessageList("de.darcoweb.varoplugin.utilities.VaroPluginLocals")
//@Getter(onMethod = @__(@Override))
public enum Messages implements MessageInfo {

    // General
    GENERAL_NEED_OP("need_op"),
    GENERAL_INVALID_ARGUMENTS("invalid"),
    GENERAL_NOT_INGAME("not_ingame"),
    GENERAL_COLOR_NOT_FOUND("no_such_color"),
    GENERAL_PROVIDE_WHOLE_NUMBER("provide_whole_number"),

    // Joining
    JOIN_NOT_PARTICIPATING("not_participating"),
    JOIN_ELIMINATED("already_eliminated"),
    JOIN_NO_TIME_LEFT("no_time_left"),

    // Game
    GAME_ALREADY_STARTED("game_already_started"),

    PLAYER_NOT_REGISTERED("player_not_registered",  "player_name"),
    PLAYER_WILL_BE_KICKED("player_will_be_kicked"),
    PLAYER_DIED("player_died",                      "player_name"),
    PLAYER_YOU_DIED("you_died"),

    PLAYER_LEFT("player_left",                      "player_name"),
    PLAYER_BUT_TIME_LEFT("but_time_left",           "time"),

    // Spawns
    SPAWN_CENTER_NOT_SET("center_unset"),
    SPAWN_ADDED("spawn_added"),
    SPAWN_REMOVED("spawn_removed",                  "id"),
    SPAWN_CENTER_SET_SUCCESS("center_set"),
    SPAWN_PROBABLY_USE_REPLACE("use_replace"),
    SPAWN_NO_SPAWNPOINT_SET("no_spawnpoint_set"),

    CAPTION_TEAMS_REGISTERED("registered_teams"),
    CAPTION_HELP("help_caption",                    "version"),

    // Teams
    TEAM_NOT_EXISTS("team_not_exists"),
    TEAM_ALREADY_EXISTS("team_already_exists"),
    TEAM_WAS_DELETED("team_deleted",                    "team_name"),
    TEAM_WAS_CREATED("team_created",                    "team_name"),
    TEAM_COLOR_CHANGED("team_color_changed",            "team_name", "color"),

    TEAM_PLAYER_NOT_IN_TEAM("player_not_in_team",       "player_name", "team_name"),
    TEAM_PLAYER_REMOVED("player_removed_from",          "player_name", "team_name"),
    TEAM_PLAYER_ADDED("player_added",                   "player_name", "team_name"),

    TEAM_WAS_ELIMINATED("team_eliminated",              "team_name"),

    // Chests
    CHEST_TEAM_ALREADY_OWNS("team_already_has_chest"),
    CHEST_NOT_YOURS("not_your_chest"),
    CHEST_MAY_NOT_PLACE_NEAR("may_not_place_near_chest"),
    CHEST_WAS_LOCKED("chest_locked"),
    CHEST_WAS_UNLOCKED("chest_unlocked"),
    CHEST_DESTROYED_BY_EXPLOSION("chest_destroyed_by_explosion"),

    // Data & Files
    CONFIG_LOCATION("config_location",                  "path"),
    CONFIG_RELOADED("config_reloaded");

    private final String key;
    private final String[] params;

    Messages(String key, String... params) {
        this.key = key;
        this.params = params;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String[] getParams() {
        return params;
    }
}
