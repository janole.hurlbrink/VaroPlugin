package de.darcoweb.varoplugin.utilities;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;

public enum GameState {
    PREPARATION(GameMode.ADVENTURE, ChatColor.AQUA, "PREPARE"), INITIAL_GRACE_PERIOD(GameMode.ADVENTURE,
            ChatColor.LIGHT_PURPLE, "GRACE"), RUNNING(GameMode.SURVIVAL, ChatColor.GREEN, "RUNNING"), ENDED(
            GameMode.CREATIVE, ChatColor.RED, "ENDED");

    private final GameMode gameMode;
    private final String name;
    private final ChatColor color;

    GameState(GameMode gm, ChatColor color, String name) {
        this.gameMode = gm;
        this.color = color;
        this.name = name;
    }

    public GameMode getGameMode() {
        return this.gameMode;
    }

    public String getRawName() {
        return this.name;
    }

    public String getName() {
        return this.color + this.name;
    }

    public ChatColor getColor() {
        return this.color;
    }
}
