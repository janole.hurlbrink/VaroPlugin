package de.darcoweb.varoplugin.utilities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents a spawn point for VARO with an {@link org.bukkit.entity.ArmorStand} marker to show a floating nametag.
 *
 * @author 1Darco1
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class Spawnpoint extends MarkedLocation implements Comparable<Spawnpoint> {

    /**
     * Creates a new Spawnpoint object from a string created with {@link #toSerializedString()}.
     *
     * @param serializedString The serialized string containing all the information
     * @return A new spawnpoint object with all fields set
     */
    public static Spawnpoint fromSerializedString(String serializedString) {
        String[] params = serializedString.split(SEPARATOR);

        UUID uuid = UUID.fromString(params[0]);
        String world = params[1];

        List<Double> numbers = Stream.of(params).skip(2).map(Double::parseDouble).collect(Collectors.toList());
        Location location = new Location(Bukkit.getWorld(world), numbers.get(0), numbers.get(1), numbers.get(2),
                numbers.get(3).floatValue(), numbers.get(3).floatValue());

        return new Spawnpoint(numbers.get(5).intValue(), location, uuid);
    }

    private final int id;

    // TODO Maybe add the Player name...
    // private String playerName;
    // private String teamName;

    public Spawnpoint(int id, @NonNull Location location, @NonNull UUID uuid) throws IllegalArgumentException {
        super(location, uuid);

        if (id <= 0) throw new IllegalArgumentException("The ID of a spawnpoint must be positive and non-null.");
        this.id = id;
    }

    public String getNametag() {
        return "Spawn #" + id;
    }

    public String toSerializedString() {
        return armorStandUuid.toString() + SEPARATOR +
                location.getWorld().getName() + SEPARATOR +
                location.getX() + SEPARATOR +
                location.getY() + SEPARATOR +
                location.getZ() + SEPARATOR +
                location.getYaw() + SEPARATOR +
                location.getPitch() + SEPARATOR +
                id;
    }

    @Override
    public int compareTo(Spawnpoint o) {
        return getId() - o.getId();
    }
}
