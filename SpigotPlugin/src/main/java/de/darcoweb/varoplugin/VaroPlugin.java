package de.darcoweb.varoplugin;

import com.google.common.reflect.ClassPath;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.event.BrewEventListener;
import de.darcoweb.varoplugin.utilities.GameState;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.manager.*;
import de.darcoweb.varoplugin.utilities.manager.FileManager.YamlFile;
import lombok.AccessLevel;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.Server;
import org.bukkit.World.Environment;
import org.bukkit.command.CommandMap;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;
import java.util.stream.Stream;

@Getter
public class VaroPlugin extends JavaPlugin {

    @Getter
    private static VaroPlugin instance;

    private FileManager fileManager;
    private TeamManager teamManager;
    private BanManager banManager;
    private TimerManager timerManager;
    private ScoreboardManager scoreboardManager;
    private SpawnManager spawnManager;
    private GameState state;

    @Getter(AccessLevel.NONE)
    private Difficulty difficulty;

    // ===== KNOWN ISSUES =====
    // - Some blacklisted enhanced potions will stay brewable with fermented spider eyes (e.g. slowness or weakness).
    // ========================

    // Great PM from @Veryo / @KrandeKraft with tons of new ideas!
    // TODO: Look through ALL files for eventual TODOs!

    // --> Priority: MINOR <--
    // TODO: More precise death messages
    // TODO: StartTimerCountdown: Adjust Tones and more...
    // (SEMI-DONE) TODO: EDITABLE MESSAGES!
    // TODO: Update german and english plugin page on dev.bukkit.org !!! --> Create Resource Page on spigotmc.org

    // #-#-#-#-#-# BUKKIT-METHODS #-#-#-#-#-#
    @Override
    public void onEnable() {
        // === Initialize Stuff ===
        instance = this;

        // Files need to be loaded first (instantiate FileManager first)
        fileManager = new FileManager();

        // Further managers refer to the BanManager so it goes second
        banManager = new BanManager();
        teamManager = new TeamManager();
        timerManager = new TimerManager();
        scoreboardManager = new ScoreboardManager();
        spawnManager = new SpawnManager(this);

        // Read from the file and init the variables.
        // assert this.state == null;
        if (!fileManager.contains(YamlFile.VARO_DATA, "GameState")) {
            this.setState(GameState.PREPARATION);
        } else {
            String stateString = (String) fileManager.get(YamlFile.VARO_DATA, "GameState");
            this.setState(GameState.valueOf(stateString));
        }

        // Now, I can register the scoreboards
        getInstance().getScoreboardManager().registerBoards();

        // === config.yml ===
        getConfig().options().copyDefaults(true);
        saveConfig();

        // === Init Locals ===
        // Moved to Message.java using VaroPlugin.getInstance()

        // === Listeners & Commands ===
        registerListeners();
        registerCommands();

        // === More Game Mechanics ===
        this.difficulty = Difficulty.valueOf(getConfig().getString("difficulty"));
        setDifficulty(difficulty);
        updateGameModes();
        updateNames();
    }

    public void onDisable() {
        getTeamManager().saveTeams();
        getBanManager().saveBans();
        getTimerManager().saveTracked();
        getSpawnManager().saveSpawns();

        this.saveConfig();
        getFileManager().saveAllFiles();

        Bukkit.getScheduler().cancelAllTasks();
    }

    private void registerListeners() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new BrewEventListener(), this);

        try {
            for (ClassPath.ClassInfo classInfo : ClassPath.from(getClassLoader())
                    .getTopLevelClasses("de.darcoweb.varoplugin.listeners")) {
                Class<?> clazz = Class.forName(classInfo.getName());

                if (Listener.class.isAssignableFrom(clazz))
                    pluginManager.registerEvents((Listener) clazz.newInstance(), this);
            }
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void registerCommands() {
        String rootCommandPackage = "de.darcoweb.varoplugin.commands.impl";
        try {
            for (ClassPath.ClassInfo classInfo : ClassPath.from(getClassLoader())
                    .getTopLevelClasses(rootCommandPackage)) {
                Class<?> clazz = Class.forName(classInfo.getName());

                if (!Command.class.isAssignableFrom(clazz)) continue;

                Optional<Constructor<?>> constructorOptional = Stream.of(clazz.getConstructors())
                        .filter(constructor -> constructor.getParameterCount() == 1)
                        .filter(constructor -> JavaPlugin.class.isAssignableFrom(constructor.getParameterTypes()[0]))
                        .findFirst();

                if (constructorOptional.isPresent()) {
                    Command command = (Command) constructorOptional.get().newInstance(this);

                    getCommandMap().register(getName(), command);

                    // Register subCommands - seems silly, but the registerSubCommand method calls itself recursively
                    registerSubCommands(command, command);
                }
            }
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void registerSubCommands(SuperCommand superCommand, Command rootCommand) {
        String superCommandPackage = superCommand.getClass().getPackage().getName();
        String subCommandPackage = superCommandPackage + "." + superCommand.getName();

        try {
            for (ClassPath.ClassInfo classInfo : ClassPath.from(getClassLoader()).getTopLevelClasses(subCommandPackage)) {
                Class<?> clazz = Class.forName(classInfo.getName());

                if (!SubCommand.class.isAssignableFrom(clazz)) continue;

                Optional<Constructor<?>> constructorOptional = Stream.of(clazz.getConstructors())
                        .filter(constructor -> constructor.getParameterCount() == 2)
                        .filter(constructor -> constructor.getParameterTypes()[0].equals(Command.class))
                        .filter(constructor -> constructor.getParameterTypes()[1].equals(SuperCommand.class))
                        .findFirst();

                if (constructorOptional.isPresent()) {
                    SubCommand subCommand = (SubCommand) constructorOptional.get().newInstance(rootCommand, superCommand);
                    superCommand.registerSubCommand(subCommand);

                    if (subCommand instanceof SuperCommand)
                        registerSubCommands((SuperCommand) subCommand, rootCommand);
                }
            }
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    // #-#-#-#-#-# GETTER-SETTER-METHODS #-#-#-#-#-#
    public CommandMap getCommandMap() {
        Class<CommandMap> targetType = CommandMap.class;

        Class<? extends Server> targetClass = getServer().getClass();

        try {
            for (Field field : targetClass.getDeclaredFields()) {
                if (targetType.isAssignableFrom(field.getType())) {
                    if (!field.isAccessible())
                        field.setAccessible(true);
                    return targetType.cast(field.get(getServer()));
                }
            }
        } catch (IllegalAccessException e) {
            throw new AssertionError("Can't happen", e);
        }

        return null;
    }

    public boolean isState(GameState state) {
        return this.state.equals(state);
    }

    public void setState(GameState state) {
        this.state = state;
        getFileManager().write(YamlFile.VARO_DATA, "GameState", this.state.toString());
    }

    // -----
    public void updateGameModes() {
        Bukkit.getOnlinePlayers().forEach(this::updateGameMode);
    }

    public void updateGameMode(Player p) {
        p.setGameMode(this.getState().getGameMode());
    }

    public void updateNames() {
        Bukkit.getOnlinePlayers().forEach(this::updateName);
    }

    public void updateName(Player p) {
        Team t = getTeamManager().getPlayersTeam(p);
        if (t != null) {
            String name = t.getColor() + p.getName() + ChatColor.RESET;
            p.setDisplayName(name);
            p.setCustomName(name);
            p.setCustomNameVisible(true);
            p.setPlayerListName(name);
        }
    }

    private void setDifficulty(Difficulty d) {
        this.difficulty = d;
        Bukkit.getWorlds().stream()
                .filter(w -> w.getEnvironment().equals(Environment.NORMAL))
                .forEach(w -> w.setDifficulty(difficulty));
    }
}
