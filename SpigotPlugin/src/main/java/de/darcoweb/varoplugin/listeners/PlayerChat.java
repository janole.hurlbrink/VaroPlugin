package de.darcoweb.varoplugin.listeners;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.manager.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChat implements Listener {

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        String msg = e.getMessage();
        Player sender = e.getPlayer();
        if (!msg.startsWith("!")) {
            TeamManager teamManager = VaroPlugin.getInstance().getTeamManager();
            Team t = teamManager.getPlayersTeam(e.getPlayer());
            if (t != null) {
                if (!t.getPlayers().isEmpty()) {
                    t.getPlayers().stream().filter(OfflinePlayer::isOnline).forEach(p ->
                                    p.getPlayer().sendMessage(ChatColor.GRAY + "TEAM | " + sender.getDisplayName()
                                            + ChatColor.GRAY + " > " + ChatColor.WHITE + msg)
                    );
                }
                e.setCancelled(true);
                return;
            }
        }

        Bukkit.broadcastMessage(ChatColor.GRAY + "GLOBAL | " + sender.getDisplayName() + ChatColor.GRAY + " > "
                + ChatColor.WHITE + ChatColor.WHITE + (msg.charAt(0) == '!' ? msg.substring(1) : msg));
        e.setCancelled(true);
    }
}
