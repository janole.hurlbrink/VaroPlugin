package de.darcoweb.varoplugin.listeners;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.GameState;
import de.darcoweb.varoplugin.utilities.Message;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.manager.BanManager;
import de.darcoweb.varoplugin.utilities.manager.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class PlayerDeath implements Listener {

    private final VaroPlugin plugin = VaroPlugin.getInstance();

    @EventHandler
    public void onEntityDeath(EntityDeathEvent e) {
        if (e.getEntity() instanceof Player && plugin.isState(GameState.RUNNING)) {
            Player p = (Player) e.getEntity();

            Chat.custom.serverBroadcast(ChatColor.RED + Message.player.DIED(p.getName()));

            BanManager banManager = VaroPlugin.getInstance().getBanManager();
            banManager.addBan(p, BanManager.OUT_OF_GAME);
            Bukkit.getScheduler().runTaskLater(VaroPlugin.getInstance(),
                    () -> p.kickPlayer(ChatColor.RED + Message.player.YOU_DIED()), 1L);

            TeamManager teamManager = VaroPlugin.getInstance().getTeamManager();
            Team team = teamManager.getPlayersTeam(p);
            if (team != null) {
                if (!team.isAlive()) {
                    Chat.custom.serverBroadcast(ChatColor.RED + Message.team.WAS_ELIMINATED(team.getName()));

                    if (teamManager.getAliveTeams().size() == 1) {
                        Team lastTeam = teamManager.getAliveTeams().iterator().next();
                        Chat.custom.serverBroadcast(ChatColor.BOLD + "" + ChatColor.GOLD + "VARO IST NUN BEENDET!");
                        Chat.custom.serverBroadcast(ChatColor.DARK_RED + "Das letzte überlebende Team ist das Team "
                                + lastTeam.getName() + " !");
                        for (OfflinePlayer offlinePlayer : lastTeam.getPlayers()) {
                            Chat.custom.serverBroadcast(ChatColor.BOLD + "" + ChatColor.DARK_AQUA
                                    + "Herzlichen Glückwunsch an " + ChatColor.YELLOW + offlinePlayer.getName()
                                    + ChatColor.DARK_AQUA + " !");
                        }
                        plugin.setState(GameState.ENDED);
                        plugin.updateGameModes();
                    }
                }
            }
        }
    }
}
