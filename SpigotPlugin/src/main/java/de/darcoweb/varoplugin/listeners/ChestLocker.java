package de.darcoweb.varoplugin.listeners;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.manager.TeamManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Iterator;

public class ChestLocker implements Listener {

    TeamManager tm = VaroPlugin.getInstance().getTeamManager();

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            Block block = e.getClickedBlock();
            if (block.getType().equals(Material.CHEST)) {
                Location loc = block.getLocation();
                tm.getTeams().stream().filter(team -> loc.equals(team.getChestLocation())).forEach(t -> {
                    Player p = e.getPlayer();
                    if (!t.getPlayers().contains(p)) {
                        Chat.err.send(p, Message.chest.NOT_YOURS());
                        e.setCancelled(true);
                    }
                });
            }
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        Block b = e.getBlockPlaced();
        Location loc = b.getLocation();
        Player p = e.getPlayer();
        Team playersTeam = tm.getPlayersTeam(p);

        // Pure laziness --> Not just block Hopper and Chest, but any block
        for (Location chestLoc : tm.getAllChests()) {
            if (chestLoc == null)
                continue;
            if (loc.toVector().distanceSquared(chestLoc.toVector()) <= 1) {
                Chat.err.send(p, Message.chest.MAY_NOT_PLACE_NEAR());
                e.setCancelled(true);
                return;
            }
        }

        if (b.getType().equals(Material.CHEST)) {
            if (playersTeam.getChestLocation() != null) {
                Chat.err.send(p, Message.chest.TEAM_ALREADY_OWNS());
                e.setCancelled(true);
                return;
            }

            playersTeam.setChestLocation(loc);
            Chat.success.send(p, Message.chest.WAS_LOCKED());
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Block b = e.getBlock();
        Location loc = b.getLocation();

        if (b.getType().equals(Material.CHEST)) {
            for (Team t : tm.getTeams()) {
                if (loc.equals(t.getChestLocation())) {
                    Player p = e.getPlayer();
                    if (!t.getPlayers().contains(p)) {
                        e.setCancelled(true);
                        Chat.err.send(p, Message.chest.NOT_YOURS());
                        return;
                    }

                    t.setChestLocation(null);
                    Chat.success.send(p, Message.chest.WAS_UNLOCKED());
                }
            }
        }
    }

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e) {
        Iterator<Block> i = e.blockList().iterator();
        while (i.hasNext()) {
            Location l = i.next().getLocation();
            if (tm.getAllChests().contains(l)) {
                Chat.info.broadcast(Message.chest.DESTROYED_BY_EXPLOSION());
                i.remove();
            }
        }
    }
}
