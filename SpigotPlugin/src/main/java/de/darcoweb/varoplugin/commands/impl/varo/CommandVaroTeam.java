package de.darcoweb.varoplugin.commands.impl.varo;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.commands.SuperSubCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import org.bukkit.command.CommandSender;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroTeam extends SuperSubCommand<VaroPlugin> {

    public CommandVaroTeam(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "team", "Manages the teams for the plugin");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 1)
            Chat.err.send(sender, Message.general.INVALID_ARGUMENTS());
        else
            super.execute(sender, args);
    }
}
