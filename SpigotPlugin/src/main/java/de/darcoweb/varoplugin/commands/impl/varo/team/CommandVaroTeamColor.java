package de.darcoweb.varoplugin.commands.impl.varo.team;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.manager.TeamManager;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroTeamColor extends SubCommand<VaroPlugin> {

    public CommandVaroTeamColor(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "color", "Changes the color of the team", "Team", "Color");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length != 2) {
            Chat.err.send(sender, Message.general.INVALID_ARGUMENTS());
            return;
        }

        TeamManager teamManager = rootCommand.getPlugin().getTeamManager();

        Team team = teamManager.getTeam(args[0]);
        if (team != null) {
            for (ChatColor color : ChatColor.values()) {
                if (color.name().equalsIgnoreCase(args[1])) {
                    team.setColor(color);
                    Chat.success.send(sender,
                            Message.team.COLOR_CHANGED(team.getName(), color.name().toLowerCase().replace("_", " ")));
                    rootCommand.getPlugin().updateNames();
                    return;
                }
            }

            // Output all existing colors to the user
            String colorString = ChatColor.BOLD + "";
            for (ChatColor c : ChatColor.values()) {
                colorString += c + c.name() + " ";
                if (c.equals(ChatColor.WHITE))
                    break;
            }

            Chat.err.send(sender, Message.general.COLOR_NOT_FOUND());
            Chat.custom.sendRaw(sender, colorString.trim());
        } else {
            Chat.err.send(sender, Message.team.NOT_EXISTS());
        }
    }
}
