package de.darcoweb.varoplugin.commands;

import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import lombok.Getter;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginIdentifiableCommand;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
@Getter//(onMethod = @__(@Override))
public abstract class Command<T extends JavaPlugin>
        extends org.bukkit.command.Command
        implements PluginIdentifiableCommand, SuperCommand {
    // Extend just Command or BukkitCommand? Implement PluginIdentifiableCommand?

    protected List<SubCommand> subCommands = new ArrayList<>();

    private final T plugin;

    private final String name;

    public Command(T plugin, String name, String description, String... aliases) {
        this(plugin, name, description, "Something went horribly wrong. Please contact the author.", aliases);
    }

    // If the aliases wouldn't have to be provided via an array directly,
    // one could not differentiate between the two constuctors.
    public Command(T plugin, String name, String description, String usage, String[] aliases) {
        super(name, description, usage, Arrays.asList(aliases));

        this.plugin = plugin;
        this.name = name;
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        String subCommandName = args.length == 0 ? "help" : args[0];
        Optional<SubCommand> subCommandOptional = subCommands.stream()
                .filter(command -> command.getName().equalsIgnoreCase(subCommandName)).findFirst();
        if (subCommandOptional.isPresent())
            subCommandOptional.get().execute(sender,
                    args.length > 1 ? Arrays.copyOfRange(args, 1, args.length) : new String[0]);
        else
            // Not sure about this... Can't change signature tho, since this is an override.
            Chat.err.send(sender, Message.general.INVALID_ARGUMENTS());

        return true;
    }
}
