package de.darcoweb.varoplugin.commands.impl.varo.team;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Message;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.manager.TeamManager;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import static org.bukkit.ChatColor.*;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroTeamList extends SubCommand<VaroPlugin> {

    public CommandVaroTeamList(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "list", "Lists all registered teams");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        TeamManager teamManager = rootCommand.getPlugin().getTeamManager();
        sender.sendMessage(GREEN + "======== " + BOLD + "" + DARK_AQUA + "["
                + Message.caption.TEAMS_REGISTERED() + "]" + RESET + "" + GREEN + " ========");

        if (!teamManager.getTeams().isEmpty()) {
            for (Team t : teamManager.getTeams()) {
                String alive = "";
                if (!t.isAlive())
                    alive = "(tot) ";
                sender.sendMessage(GREEN + "- " + alive + YELLOW + t.getName()
                        + GREEN + " with " + t.getSize() + " player(s):");
                for (OfflinePlayer p : t.getPlayers()) {
                    sender.sendMessage(GREEN + "    * " + YELLOW + p.getName());
                }
            }
        }
        sender.sendMessage(GREEN + "======== ================= ========");
    }
}
