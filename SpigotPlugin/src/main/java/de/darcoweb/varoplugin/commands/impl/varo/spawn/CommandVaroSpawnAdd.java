package de.darcoweb.varoplugin.commands.impl.varo.spawn;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroSpawnAdd extends SubCommand<VaroPlugin> {

    // TODO support boolean inHole

    public CommandVaroSpawnAdd(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "add", "Adds a spawn point manually");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        rootCommand.getPlugin().getSpawnManager().addSpawn(((Player) sender).getLocation(), false);
        Chat.success.send(sender, Message.spawn.ADDED());
    }
}
