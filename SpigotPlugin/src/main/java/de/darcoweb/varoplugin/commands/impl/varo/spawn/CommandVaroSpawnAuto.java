package de.darcoweb.varoplugin.commands.impl.varo.spawn;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.MarkedLocation;
import de.darcoweb.varoplugin.utilities.Message;
import de.darcoweb.varoplugin.utilities.StringUtils;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroSpawnAuto extends SubCommand<VaroPlugin> {

    // TODO Cover different options (like auto-mark, auto-build, -precise-coords / -block-coords
    public CommandVaroSpawnAuto(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "auto", "Automagically sets all spawn points based on the center", "amount", "range");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        MarkedLocation.MutableMarkedLocation center = rootCommand.getPlugin().getSpawnManager().getCenter();
        if (args.length < 2) {
            Chat.err.send(sender, Message.general.INVALID_ARGUMENTS());
            return;
        } else if (!center.isLocationSet()) {
            Chat.err.send(sender, Message.spawn.CENTER_NOT_SET());
            return;
        }

        Optional<Integer> amount = StringUtils.parseSignedInteger(args[0]);
        Optional<Integer> range = StringUtils.parseUnsignedInteger(args[1]);

        if (!amount.isPresent() || !range.isPresent() || amount.get() <= 0) {
            Chat.err.send(sender, Message.general.PROVIDE_WHOLE_NUMBER());
            return;
        }

        Location centerLocation = center.getLocation();
        List<Location> locations = new ArrayList<>();

        int radius = range.get(), pointCount = amount.get();
        double theta = 2.0 * Math.PI / (double) pointCount;

        for (int i = 0; i < pointCount; i++) {

            // using a circle like the unit circle but with r = radius
            double angle = theta * i;
            double pointX = Math.cos(angle) * radius;
            double pointY = Math.sin(angle) * radius;

            // transferring the coordinates into the actual minecraft coordinate system
            double x = centerLocation.getX() + pointX;
            double y = centerLocation.getY();
            double z = centerLocation.getZ() + pointY;
            float yaw = (float) Math.toDegrees(angle) + 90.f; // 0 is not positive x but rather positive z in minecraft
            float pitch = 0f;

            locations.add(new Location(centerLocation.getWorld(), x, y, z, yaw, pitch));
        }

        locations.forEach(location -> rootCommand.getPlugin().getSpawnManager().addSpawn(location, false));
    }
}
