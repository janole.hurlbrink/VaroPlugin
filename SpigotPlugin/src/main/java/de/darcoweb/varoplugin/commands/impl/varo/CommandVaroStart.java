package de.darcoweb.varoplugin.commands.impl.varo;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.runnables.StartTimer;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.GameState;
import de.darcoweb.varoplugin.utilities.Message;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.manager.TeamManager;
import de.darcoweb.varoplugin.utilities.manager.TimerManager;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroStart extends SubCommand<VaroPlugin> {

    public CommandVaroStart(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "start", "Starts the game");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        VaroPlugin plugin = rootCommand.getPlugin();
        if (plugin.getState().equals(GameState.PREPARATION)) {
            StartTimer startTimer = new StartTimer(plugin);

            testForUnregisteredPlayers();
            TimerManager timerManager = plugin.getTimerManager();
            timerManager.runStartTimer(plugin, startTimer);
        } else {
            Chat.err.send(sender, Message.game.ALREADY_STARTED());
        }
    }

    private void testForUnregisteredPlayers() {
        int unregistered = 0;
        for (Player player : Bukkit.getOnlinePlayers()) {
            TeamManager teamManager = rootCommand.getPlugin().getTeamManager();
            Team team = teamManager.getPlayersTeam(player);
            if (team == null) {
                Chat.err.serverBroadcast(Message.player.NOT_REGISTERED(player.getName()));
                unregistered++;
            }
        }

        if (unregistered > 0 && !rootCommand.getPlugin().getConfig().getBoolean("allow_unregistered_players"))
            Chat.err.opBroadcast(Message.player.WILL_BE_KICKED());
    }
}
