package de.darcoweb.varoplugin.commands.impl.varo;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import org.bukkit.command.CommandSender;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroConfig extends SubCommand<VaroPlugin> {

    public CommandVaroConfig(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "config", "Show information about the config.yml");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        Chat.info.send(sender, Message.config.LOCATION(rootCommand.getPlugin().getDataFolder().getPath()));
    }
}
