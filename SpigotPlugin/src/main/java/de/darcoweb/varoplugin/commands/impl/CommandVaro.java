package de.darcoweb.varoplugin.commands.impl;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import org.bukkit.command.CommandSender;

public class CommandVaro extends Command<VaroPlugin> {

    public CommandVaro(VaroPlugin plugin) {
        super(plugin, "varo", "Manages the Varo plugin.", "v");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        if (!sender.isOp()) {
            Chat.err.send(sender, Message.general.NEED_OP());
            return true;
        }

        return super.execute(sender, alias, args);
    }
}
