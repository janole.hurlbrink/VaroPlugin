package de.darcoweb.varoplugin.commands;

import lombok.Getter;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
@Getter
public abstract class SubCommand<T extends JavaPlugin> {

    protected final Command<T> rootCommand;
    protected final SuperCommand superCommand;
    protected final String name;
    protected final String description;
    protected final String[] parameters;

    public SubCommand(Command<T> rootCommand, SuperCommand superCommand, String name, String description, String... parameters) {
        this.rootCommand = rootCommand;
        this.superCommand = superCommand;
        this.name = name;
        this.description = description;
        this.parameters = parameters;
    }

    public abstract void execute(CommandSender sender, String[] args);

    public String getFullCommand() {
        String fullCommand = name;
        SuperCommand superCmd = superCommand;

        while (superCmd instanceof SubCommand) {
            fullCommand = superCmd.getName() + " " + fullCommand;
            superCmd = ((SubCommand) superCmd).getSuperCommand();
        }

        return superCmd.getName() + " " + fullCommand;
    }
}
