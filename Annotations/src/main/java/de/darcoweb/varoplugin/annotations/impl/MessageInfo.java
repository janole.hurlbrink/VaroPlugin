package de.darcoweb.varoplugin.annotations.impl;

/**
 * Has to be used toghether with the {@link de.darcoweb.varoplugin.annotations.MessageList @MessageList} annotation.
 * This interface is used to supply the data to the code generator.
 *
 * @author 1Darco1
 */
public interface MessageInfo {

    String getKey();

    String[] getParams();
}
