package de.darcoweb.varoplugin.annotations;

import java.lang.annotation.*;

/**
 * Indicates an enum that contains information about messages. Supports parameters like #{player_name}.
 * Will generate Message class with public static final constants.
 *
 * The information needs to be supplied through the public methods 'key()' (String) and 'params()' (String[]).
 * Use the {@link de.darcoweb.varoplugin.annotations.impl.MessageInfo MessageInfo} interface.
 *
 * @author 1Darco1
 */
@Inherited
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.TYPE)
public @interface MessageList {

    // fullQualifiedName to message locals.
    String value();

    String destinationPackage() default "";
}
